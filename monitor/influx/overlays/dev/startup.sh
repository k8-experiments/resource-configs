#!/bin/bash -x

sleep 5;
influx bucket create -n logs -r 1d -o none -t $DOCKER_INFLUXDB_INIT_ADMIN_TOKEN || true
influx apply -o none -t $DOCKER_INFLUXDB_INIT_ADMIN_TOKEN -f home/influx/dashboard_log.yaml --force true || true

