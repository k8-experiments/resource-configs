# k8 Resource config

This is a collection of Kubernetes resources configurations.
They are intended to serve as a starting point to get "services" into a cluster
The configurations are either kustomizations or helmfiles.

*The configurations are supposed to "work" with each other but this might not always be the case*

## "services"

###  ingress
 
 - traefik

### db

 - postgresSQl
 - pgadmin4


### api
 
 - hasura


### misc services
 
 - echo

