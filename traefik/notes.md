# General notes

## example k8 env

### k3d 

```bash
k3d cluster create  \
	--k3s-arg "--disable=traefik@server:0" \
	-p 80:80@loadbalancer \
	-p 443:443@loadbalancer 
```

## files

 - `helmfile.yaml` > for deployment
 - `ingres.yaml` > for dashbaord and api ingres


## ingres

http://traefik.localhost/dashboard/